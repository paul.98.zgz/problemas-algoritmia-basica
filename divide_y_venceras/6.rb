#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3


ejemplo = [9, 13, 16, 18, 19, 23, 28, 31, 37, 42, -4, 0, 2, 5, 7, 8]

N = ejemplo.length

def numeroDeRotaciones(a)
  if a[0] > a[1]
    return 1
  else
    return 1 + numeroDeRotaciones(a[1..])
  end
end

puts numeroDeRotaciones(ejemplo)