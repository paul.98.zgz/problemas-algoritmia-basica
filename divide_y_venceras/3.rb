#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3

require 'matrix'

imagen = Matrix[
    [0, 0, 0, 0],
    [0, 0, 1, 0],
    [0, 1, 0, 1],
    [1, 0, 0, 0]
]

def rotar(m)
  # [                    [
  #  [m1, m2],    ==>     [m4, m1],
  #  [m4, m3]     ==>     [m3, m2]
  # ]                    ]

  tam = m.row_count

  if tam == 2       # ¿La matríz es 2x2?
    m1 = m[0,0]
    m2 = m[0,1]
    m3 = m[1,1]
    m4 = m[1,0]

    return Matrix[
        [m4, m1],
        [m3, m2]
    ]
  else
    m1 = rotar(m.minor(0...tam/2, 0...tam/2))
    m2 = rotar(m.minor(0...tam/2, tam/2...tam))
    m3 = rotar(m.minor(tam/2...tam, tam/2...tam))
    m4 = rotar(m.minor(tam/2...tam, 0...tam/2))

    return Matrix.vstack(
        Matrix.hstack(m4,m1),
        Matrix.hstack(m3,m2)
    )
  end
end

puts rotar(imagen)