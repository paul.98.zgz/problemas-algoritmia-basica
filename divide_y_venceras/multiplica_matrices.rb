#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3

require 'matrix'

A = Matrix[
    [1, 2, 0, 1],
    [2, 1, 1, 0],
    [2, 2, 1, 2],
    [1, 0, 1, 2]
]

B = Matrix[
    [1, 2, 2, 2],
    [2, 1, 2, 0],
    [0, 2, 1, 2],
    [1, 1, 0, 1]
]

def multiplica_matrices(a, b)
  tam = a.row_count

  if tam == 2
    a11 = a[0,0]
    a12 = a[0,1]
    a21 = a[1,0]
    a22 = a[1,1]

    b11 = b[0,0]
    b12 = b[0,1]
    b21 = b[1,0]
    b22 = b[1,1]

    m1 = (a21 + a22 - a11) * (b22 - b12 + b11)
    m2 = (a11) * (b11)
    m3 = (a12) * (b21)
    m4 = (a11 - a21) * (b22 - b12)
    m5 = (a21 + a22) * (b12 - b11)
    m6 = (a12 - a21 + a11 - a22) * (b22)
    m7 = (a22) * (b11 + b22 - b12 - b21)

    result = Matrix[[m2 + m3, m1 + m2 + m5 + m6], [m1 + m2 + m4 - m7, m1 + m2 + m4 + m5]]
  else
    a11 = a.minor(0...tam/2, 0...tam/2)
    a12 = a.minor(0...tam/2, tam/2...tam)
    a21 = a.minor(tam/2...tam, 0...tam/2)
    a22 = a.minor(tam/2...tam, tam/2...tam)

    b11 = b.minor(0...tam/2, 0...tam/2)
    b12 = b.minor(0...tam/2, tam/2...tam)
    b21 = b.minor(tam/2...tam, 0...tam/2)
    b22 = b.minor(tam/2...tam, tam/2...tam)

    m1 = multiplica_matrices(a21 + a22 - a11, b22 - b12 + b11)
    m2 = multiplica_matrices(a11, b11)
    m3 = multiplica_matrices(a12, b21)
    m4 = multiplica_matrices(a11 - a21, b22 - b12)
    m5 = multiplica_matrices(a21 + a22, b12 - b11)
    m6 = multiplica_matrices(a12 - a21 + a11 - a22, b22)
    m7 = multiplica_matrices(a22, b11 + b22 - b12 - b21)

    result = Matrix.vstack(Matrix.hstack(m2 + m3, m1 + m2 + m5 + m6), Matrix.hstack(m1 + m2 + m4 - m7, m1 + m2 + m4 + m5))
  end


  return result
end

puts multiplica_matrices(A, B)