#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3

lista = [-3, -1, 0, 0, 1, 4, 5, 8, 10, 11, 13, 14, 15]
total = lista.length

def indice_igual_elemento(l, n, base)

    if n == 0
        return ""
    end

    if l[n/2] == base + n/2 + 1
        return l[n-1]    
    elsif l[n/2] > base + n/2 + 1
        return indice_igual_elemento(l[0, n/2], n/2, base)
    else
        return indice_igual_elemento(l[(n - n/2), n], (n - n/2) - 1, (n - n/2) + base)
    end
end

puts indice_igual_elemento(lista, total, 0)