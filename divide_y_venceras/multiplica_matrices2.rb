#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3

require 'matrix'

A = Matrix[
    [1, 2, 0, 1],
    [2, 1, 1, 0],
    [2, 2, 1, 2],
    [1, 0, 1, 2]
]

B = Matrix[
    [1, 2, 2, 2],
    [2, 1, 2, 0],
    [0, 2, 1, 2],
    [1, 1, 0, 1]
]

def multiplica_matrices(a, b)
  return a*b
end

puts multiplica_matrices(A, B)