#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3

conjunto_reales = [1.0, 2.3, 3.5, 0.7, 3.8, 3.3, 2.1, 1.9, 5.5, 3,6]

# def busca_suma(l, n)
#   l.each_with_index do |numero, i|
#     for numero2 in l[i+1..]
#       if numero + numero2 == n
#         puts numero, numero2
#         return
#       end
#     end
#   end
# end
#
# busca_suma(conjunto_reales,6.2)


#######################################################################

conjunto_reales.sort!

def busca_suma(l, n, min, max)
  if l[min] + l[max] == n
    puts l[min], l[max]
  elsif l[min] + l[max] < n
    busca_suma(l, n, min+1, max)
  else
    busca_suma(l, n, min, max-1)
  end
end

busca_suma(conjunto_reales, 6.2, 0, conjunto_reales.length - 1)