#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3

paradas = {
    a: 3,
    b: 2,
    c: 4,
    d: 4,
    e: 5,
    f: 1,
    g: 2,
    h: 3,
    z: 2
}


deposito = 6

cuenta = 0
ultima_parada = nil

for parada in paradas
    if parada[1] + cuenta <= deposito
        cuenta += parada[1]
    else
        puts ultima_parada[0]
        cuenta = parada[1]
    end
    ultima_parada = parada
end