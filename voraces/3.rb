#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3

Objeto = Struct.new(:peso,:valor)

inventario = [
    Objeto.new(4,50),
    Objeto.new(3,60),
    Objeto.new(6,30),
    Objeto.new(7,20),
    Objeto.new(8,10),
    Objeto.new(1,80),
    Objeto.new(5,40),
    Objeto.new(6,30),
    Objeto.new(4,50),
    Objeto.new(2,70),
    Objeto.new(6,30)
]

mochila = []

capacidad = 20

inventario.sort! do | a, b | a[:peso] <=> b[:peso] end

for objeto in inventario
    if objeto.peso < capacidad
        mochila << objeto
        capacidad -= objeto.peso
    end
end

puts mochila