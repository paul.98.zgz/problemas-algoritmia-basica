#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3

Coord = Struct.new(:x,:y)

grupo = [
    Coord.new(2,1),
    Coord.new(4,2),
    Coord.new(1,3),
    Coord.new(3,3),
    Coord.new(5,3),
    Coord.new(4,4),
    Coord.new(3,5),
    Coord.new(3,6),
]

grupo.sort! do | a, b | a[:y] <=> b[:y] end
grupo.sort! do | a, b | a[:x] <=> b[:x] end

grupo.reverse!

maximales = [grupo[0]]

for coord in grupo
    next if coord == grupo[0]
    if coord.x == maximales.last.x and coord.y < maximales.last.y
        next
    elsif coord.y > maximales.last.y
        maximales << coord
    end
end

puts maximales