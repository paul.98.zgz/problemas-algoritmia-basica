#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3
#
# Basado en la solución propuesta por el profesor Campos, disponible en
# https://moodle2.unizar.es/add/pluginfile.php/2567606/mod_forum/attachment/465986/1-voraces-7.pdf

alumnos = [1.68, 1.73, 1.69, 1.81, 1.78, 1.92, 1.85, 1.69, 1.72, 1.80]

esquis  = [1.80, 1.75, 1.65, 1.90, 1.70, 1.60, 1.65, 1.70, 1.80, 1.90]


def asignar(a, e)
  a.sort!                 # Coste => O(n·log(n))
  e.sort!                 # Coste => O(n·log(n))

  diferencial_total = 0.0

  for i in (0...a.length) # Coste => O(n)
    puts "Alumno de altura: #{a[i]}; Esqui de tamanyo: #{e[i]}"
    diferencial_total += (a[i] - e[i]).abs
  end

  puts
  puts "Diferencial total: #{diferencial_total.round(2)}"
end

asignar(alumnos, esquis)
