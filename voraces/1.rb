#!/usr/bin/env ruby
# Autor: Paul Hodgetts Isarría
# Licencia: GNU GPLv3

cesta = {
  manzana: 1,
  zanahoria: 5,
  patata: 1,
  naranja: 2,
  kiwi: 3,
  tamarindo: 4,
  mandarina: 2,
  coco: 10,
  papaya: 7,
  pera: 2,
  melon: 10,
  sandia: 9,
  arandanos: 6,
  platano: 3,
  mango: 2,
  cerezas: 6,
  fresas: 7
}

cestaB = {
  manzana: 1,
  zanahoria: 2,
  patata: 3,
  naranja: 4,
  kiwi: 4,
  tamarindo: 4,
  mandarina: 4,
  coco: 4,
  papaya: 4,
  pera: 4,
  melon: 5,
  sandia: 6,
  arandanos: 7,
  platano: 8,
  mango: 9,
  cerezas: 10,
  fresas: 11
}

cesta = cesta.sort_by do |_key, value| value end

comprado = []
regalado = []

comprar = true

for producto in cesta.reverse
  if comprar
    comprado += producto
  else
    regalado += producto
  end

  comprar = !comprar
end

puts "=*=*= Comprado =*=*="
puts comprado
puts
puts "=*=*= Regalado =*=*="
puts regalado