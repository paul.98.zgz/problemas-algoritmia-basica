Problemas de Algoritmia Básica (2019-2020)
==========================================

Estas son mis soluciones a los problemas de la asignatura "Algoritmia Básica" de la Universidad de Zaragoza, curso 2019-2020.

![Made with Ruby](https://img.shields.io/badge/Made%20with-Ruby-red?style=plastic&logo=ruby)
